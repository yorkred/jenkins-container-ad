# Jenkins in a container

## Overview

Jenkins in a container with active-directory plugin.

## Publishing docker image

```bash
export DOCKER_ID_USER="docker hub username"
docker login
docker push $DOCKER_ID_USER/jenkins-ad
```

## Deploying to OpenShift

#1
* Log-in using CLI: `./oc/oc login
* Specify login url, e.g. `https://console.starter-ca-central-1.openshift.com`
* The url is the same as when using a OpenShift web console
* Type username
* Type password
* Create a new project:
```
./oc/oc new-project jenkins-ad-project --description="Jenkins with AD, Running on OpenShift" --display-name="Jenkins AD project"
```
or via web-admin.


#2 
* Login in web-admin and create volume:
 - Storaje class: ebs
 - Name: vol1
 - Access mode: RWO

! Important ! Volume name (vol1) is used to map this volume inside container to /var/jenkins_home

#3
* Deploy a pod from pod_ad.json: ./oc/oc create -f pod_ad.json
* Deploy a service for this pod: ./oc/oc create -f service.json
* Go to web-admin: Applications -> Services -> jenkins-ad-service
  and click "create route"


NOTES:
  File pod_ad.json contain enviroment variables:

  - Java additional startup options:
    JAVA_OPTS="-Djenkins.install.runSetupWizard=false -Xms512M -Xmx512M" 
    !Important! Set correct memory limits here, or jenkins process will be killed by openshift.

  - AD authentication parameters:
    AD_NAME="some.domain.name" 
    AD_SERVER="some.domain.name:389" (389 - port number, optional)
    AD_LOGIN='CN=loginname,CN=Group,DC=some,DC=domain,DC=name'
    AD_PASSWORD='somepassword'

  - Jenkins admin account (login from AD)
    JENKINS_USER="jenkins.user"

  - optional:
   HOME="/var/jenkins_home" \
